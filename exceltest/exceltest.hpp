/*
 Example C++ class, showing how to build a shared library
 for use with Excel in Office 365 for Mac.
 */


//This is just an include guard.
#ifndef __exceltestlibrary__
#define __exceltestlibrary__

//Start of visibility attributes.
#pragma GCC visibility push(default)

//By declaring extern C, the compiler won't mangle the function names,
//and it's exported with the understanding of C linkage.
extern "C" {
    int add(int a, int b);
    int answerToLifeUniverseAndEverything(void);
}

//End of visibility attributes
#pragma GCC visibility pop

#endif
